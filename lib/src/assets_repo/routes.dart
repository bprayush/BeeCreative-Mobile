class Routes {
  static const SPLASH_SCREEN = '/';
  static const TUTORIAL = '/tutorial';
  static const GOOGLE_LOGIN = '/login/google';
  static const EMAIL_LOGIN = '/login/email';
  static const NOTIFICATION_SETUP = '/app/notification/setup';
  static const WELCOME_BACK = '/app/welcome-back';
  static const SCHEDULES = '/app/schedules';
  static const UNDER_CONSTRUCTION = '/app/under-construction';
  static const CLASS_DETAILS = 'app/schedules/class-details';
}
